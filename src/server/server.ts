import http, {Server as HttpServer}  from 'http';
import express, {Application} from 'express';
import cors from 'cors';
import { Routes } from '../routes/routes';
import SocketIOServer from './socketIOServer';

export class Server{
    private server: HttpServer;
    private port = process.env.PORT || 3001;
    private routes = new Routes();
    private socketServer: SocketIOServer;
    
    public initServer() :void{
        const app: Application = express();                
        this.server! = http.createServer(app);

        app.use(cors());
        app.use(this.routes.router);
        
        this.socketServer = new SocketIOServer(this.server);
        this.socketServer.initConnection();
        
        this.server.listen(this.port, ()=>console.log('listening on port ', this.port));
    }
}