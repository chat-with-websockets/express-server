import socketio, { Socket } from 'socket.io';
import { Server as HttpServer } from 'http';
import UserService, { IUser } from '../services/userService';

export default class SocketIOServer {
    private io: socketio.Server;

    constructor(httpServer: HttpServer) {
        this.createServer(httpServer);
    }

    private createServer(httpServer: HttpServer): void {
        this.io = new socketio.Server(httpServer, {
            cors: {
                origin: 'http://localhost:3000', // '*'
                methods: ['GET', 'POST'],
                credentials: true,
            },
        });
    }

    public initConnection(): void {
        this.io.on('connection', (socket: Socket) => {
            console.log('user joins!');

            this.onJoin(socket);
            this.onSendMessage(socket);
            this.onDisconnect(socket);
        });
    }

    private onJoin(socket: Socket): void {
        socket.on('join', ({ name, room }, callback) => {
            const { error, user } = UserService.addUser({
                id: socket.id,
                name,
                room,
            });
            if (error) return callback(error);

            this.emit(socket, 'message', {
                user: 'admin',
                text: `${user.name}, welcome to the room ${user.room}`,
            });
            this.emit(socket.broadcast.to(user.room), 'message', {
                user: 'admin',
                text: `${user.name} has joined to the room`,
            });

            socket.join(user.room);
            this.emit(this.io.to(user.room), 'roomData', {
                room: user.room,
                users: UserService.getUsersInRoom(user.room),
            });

            callback();
        });
    }

    private onSendMessage(socket: Socket): void {
        socket.on('sendMessage', (message: string, callback: Function) => {
            try {
                const user = <IUser>UserService.getUser(socket.id);
                this.emit(this.io.to(user.room), 'message', {
                    user: user.name,
                    text: message,
                });
                callback();
            } catch (error) {
                console.log(error);
            }
        });
    }

    private emit(
        sender: socketio.BroadcastOperator<any> | socketio.Socket<any>,
        connString: string,
        data: any
    ): void {
        sender.emit(connString, data);
    }

    private onDisconnect(socket: Socket): void {
        socket.on('disconnect', () => {
            console.log('user has left');

            const user = UserService.removeUser(socket.id);
            if (!user) return null;

            this.emit(this.io.to(user.room), 'message', {
                user: 'admin',
                text: `${user.name} has left the room`,
            });
            this.emit(this.io.to(user.room), 'roomData', {
                room: user.room,
                users: UserService.getUsersInRoom(user.room),
            });
        });
    }
}
