import { Request, Response, Router } from 'express';

export class Routes{
    private _router: Router;

    constructor(){
        this._router = Router();
        this.initRoutes();
    }

    initRoutes():void{
        this._router.get('/', (req: Request, res: Response)=>{
            res.send('App up and running');
        });
    }

    get router(): Router{
        return this._router;
    }
}