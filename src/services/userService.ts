
export interface IUser{
    id: string,
    name: string,
    room: string
}

export default class UserService {
    private static users: IUser[] = [];

    public static addUser({ id, name, room }: IUser): IUser | any  {
        const nameTrimed = (name as string).trim().toLowerCase();
        const roomTrimed = (room as string).trim().toLowerCase();

        const userFounded = UserService.users.find(
            (user) => user.room === room && user.name === name
        );
        if (userFounded) return { error: 'username is taken' };

        const user = { id, name: nameTrimed, room: roomTrimed };
        UserService.users.push(user);

        return { user };
    }

    public static removeUser (id: any) {
        const index = UserService.users.findIndex((user) => user.id === id);
    
        if (index == -1) return;
    
        return UserService.users.splice(index, 1)[0];
    }

    public static getUser (id: any) {
        return UserService.users.find((user) => user.id === id);
    }
    
    public static getUsersInRoom (room: string) {
        return UserService.users.filter((users) => users.room === room);
    }
}
